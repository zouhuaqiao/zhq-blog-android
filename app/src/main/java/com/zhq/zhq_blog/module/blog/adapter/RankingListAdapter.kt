package com.zhq.zhq_blog.module.blog.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.zhq.zhq_blog.R
import com.zhq.zhq_blog.module.blog.bean.BlogPageListResult

class RankingListAdapter(data: MutableList<BlogPageListResult.Data>?) : BaseQuickAdapter<BlogPageListResult.Data, BaseViewHolder>(R.layout.rv_item_ranking_list, data)  {

    private var i = 1
    private var listI = arrayListOf<Int>()

    fun setI(int: Int){
        i = int
    }

    override fun convert(holder: BaseViewHolder, item: BlogPageListResult.Data) {
        listI.add(i)
        i++
        holder.setText(R.id.tv_ranking,listI[getItemPosition(item)].toString())
        holder.setText(R.id.tv_ranking_title,item.title)
    }

}