package com.zhq.zhq_blog.module.blog.bean

/**
 *
 */
data class EditBlogRequest(
    var content: String = "", // string
    var id: Int = 0, //博客id
    var title: String = "" // string
)