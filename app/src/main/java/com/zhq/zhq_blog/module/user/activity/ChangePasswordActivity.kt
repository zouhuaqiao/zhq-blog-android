package com.zhq.zhq_blog.module.user.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.jeremyliao.liveeventbus.LiveEventBus
import com.zhq.zhq_blog.base.BaseVMActivity
import com.zhq.zhq_blog.databinding.ActivityChangePasswordBinding
import com.zhq.zhq_blog.module.bus.UpdateUserInfoEvent
import com.zhq.zhq_blog.module.user.viewmodel.ChangePasswordViewModel
import com.zhq.zhq_blog.util.zhq.toast

class ChangePasswordActivity : BaseVMActivity<ChangePasswordViewModel>() {
    val mViewBinding by lazy { ActivityChangePasswordBinding.inflate(layoutInflater) }

    companion object {
        fun startActivity(context: Context) {
            val intent = Intent(context, ChangePasswordActivity::class.java)
            context.startActivity(intent)
        }

    }

    override fun setContentView() {
        setContentView(mViewBinding.root)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTitle()
        mViewBinding.tvSave.setOnClickListener {
            val trim = mViewBinding.etPassword.text.toString().trim()
            if (trim.isEmpty()) {
                toast("用户密码不能为空")
            } else {
                netChangePassword(trim)
            }
        }
    }

    private fun netChangePassword(trim: String) {
        mViewModel.changePassword(trim).observe(this) {
            if (it.isServerResultOK()) {
                toast("修改成功")
                UpdateUserInfoEvent().apply {
                    LiveEventBus.get(UpdateUserInfoEvent::class.java).post(this)
                }
                finishAfterTransition()
            } else {
                toast("修改失败")
            }
        }
    }


    private fun initTitle() {
        mViewBinding.layoutTitle.ivBack.setOnClickListener {
            finishAfterTransition()
        }
        mViewBinding.layoutTitle.tvTitle.text = "修改用户密码"
    }
}