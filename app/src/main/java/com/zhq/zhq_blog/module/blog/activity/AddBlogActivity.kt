package com.zhq.zhq_blog.module.blog.activity

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityOptionsCompat
import com.zhq.zhq_blog.R
import com.zhq.zhq_blog.base.BaseVMActivity
import com.zhq.zhq_blog.databinding.ActivityAddBlogBinding
import com.zhq.zhq_blog.module.blog.markdown.EditorAction
import com.zhq.zhq_blog.module.blog.viewmodel.AddBlogViewModel
import com.zhq.zhq_blog.util.zhq.toast

class AddBlogActivity : BaseVMActivity<AddBlogViewModel>(), View.OnClickListener {
    val mViewBinding by lazy { ActivityAddBlogBinding.inflate(layoutInflater) }

    private lateinit var editorAction: EditorAction

    companion object {

        fun startActivity(context: Context) {
            val intent = Intent(context, AddBlogActivity::class.java)
            context.startActivity(intent)
        }

        /**
         * view是需要共享效果的view
         * 否则不会报错,但没有动画效果
         */
        fun startActivityForTransition(activity: Activity, view: View) {
            //共享元素跳转
            val i = Intent(activity, AddBlogActivity::class.java)
            val pair1 = androidx.core.util.Pair(view, "SHARE_VIEW_ADD")
            val optionsCompat: ActivityOptionsCompat =
                ActivityOptionsCompat.makeSceneTransitionAnimation(activity, pair1)
            // ActivityCompat是android支持库中用来适应不同android版本的
            ActivityCompat.startActivity(activity, i, optionsCompat.toBundle())
        }
    }

    private fun initView() {
        mViewBinding.toolbar.title = "编辑博客"
        setSupportActionBar(mViewBinding.toolbar)
        editorAction = EditorAction(context, mViewBinding.etContent)
        mViewBinding.etContent.requestFocus()
        setOnClickListener()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.edit_fragment_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.undo -> editorAction.undo()
            R.id.redo -> editorAction.redo()
            R.id.preview -> preview()
            R.id.fabu -> release()
            R.id.clear_all -> editorAction.clearAll()
            R.id.md_docs -> editorAction.checkDocs()
            R.id.statistics -> editorAction.statistics()
            R.id.exit -> finish()

        }
        return true

    }

    private fun preview(){
        editorAction.toggleKeyboard(0)
        val intent = Intent(this,PreviewBlogActivity::class.java)
        intent.putExtra("content",mViewBinding.etContent.text.toString())
        context.startActivity(intent)
    }

    override fun setContentView() {
        setContentView(mViewBinding.root)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    private fun release(){
        val title = mViewBinding.etTitle.text.toString()
        val content = mViewBinding.etContent.text.toString()
        when {
            title.isEmpty() -> toast("标题不能为空")
            content.isEmpty() -> toast("内容不能为空")
            else -> {
                netAdd(title, content)
            }
        }
    }

    private fun netAdd(title: String, content: String) {
        mViewModel.add(title, content).observe(this) {
            if (it.isServerResultOK()) {
                finish()
            }
        }
    }

    fun setOnClickListener() {
        mViewBinding.editorActionbar.heading.setOnClickListener(this)
        mViewBinding.editorActionbar.bold.setOnClickListener(this)
        mViewBinding.editorActionbar.italic.setOnClickListener(this)
        mViewBinding.editorActionbar.code.setOnClickListener(this)
        mViewBinding.editorActionbar.quote.setOnClickListener(this)
        mViewBinding.editorActionbar.listNumber.setOnClickListener(this)
        mViewBinding.editorActionbar.listBullet.setOnClickListener(this)
        mViewBinding.editorActionbar.link.setOnClickListener(this)
        mViewBinding.editorActionbar.image.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                mViewBinding.editorActionbar.heading.id -> editorAction.heading()
                mViewBinding.editorActionbar.bold.id -> editorAction.bold()
                mViewBinding.editorActionbar.italic.id -> editorAction.italic()
                mViewBinding.editorActionbar.code.id -> editorAction.insertCode()
                mViewBinding.editorActionbar.quote.id -> editorAction.quote()
                mViewBinding.editorActionbar.listNumber.id -> editorAction.orderedList()
                mViewBinding.editorActionbar.listBullet.id -> editorAction.unorderedList()
                mViewBinding.editorActionbar.link.id -> editorAction.insertLink()
                mViewBinding.editorActionbar.image.id -> handleImage()
            }
        }
    }

    private fun handleImage() {
        //insert image
        val inputDialog: AlertDialog.Builder = AlertDialog.Builder(context)
        inputDialog.setTitle(R.string.dialog_title_insert_image)
        val inflater: LayoutInflater = layoutInflater
        val dialogView: View = inflater.inflate(R.layout.dialog_insert_image, null)
        inputDialog.setView(dialogView)
        inputDialog.setNegativeButton(R.string.cancel
        ) { dialog, _ -> dialog.cancel() }

        inputDialog.setPositiveButton(R.string.dialog_btn_insert
        ) { _, _ ->
            val imageDisplayText = dialogView.findViewById<EditText>(
                R.id.image_display_text
            )
            val imageUri = dialogView.findViewById<EditText>(R.id.image_uri)
            editorAction.insertImage(
                imageDisplayText.text.toString(),
                imageUri.text.toString()
            )
        }
        inputDialog.show()
    }

}