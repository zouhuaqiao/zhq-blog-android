package com.zhq.zhq_blog.module.blog.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import com.zhq.zhq_blog.R
import com.zhq.zhq_blog.base.BaseFragment
import com.zhq.zhq_blog.databinding.FragmentTopTabBinding
import com.zhq.zhq_blog.module.blog.activity.AddBlogActivity
import com.zhq.zhq_blog.module.blog.activity.SearchBlogActivity
import com.zhq.zhq_blog.module.mian.adapter.MyMainPagerAdapter
import com.zhq.zhq_blog.util.log.LogUtil

class TopTabFragment : BaseFragment() {
    lateinit var mViewBinding:FragmentTopTabBinding
    private var fragmentList:ArrayList<Fragment> = ArrayList()
    private var titles:List<String> = listOf("最新","排行榜")

    override fun createView(inflater: LayoutInflater?, container: ViewGroup?): View {
        mViewBinding = FragmentTopTabBinding.inflate(inflater!!,container,false)
        return mViewBinding.root
    }

    override fun onLazyLoad() {
        setTopTabAndViewPager()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewBinding.ivAdd.setOnClickListener {
            AddBlogActivity.startActivityForTransition(requireActivity(), mViewBinding.ivAdd)
        }
        mViewBinding.llSearch.setOnClickListener {
            SearchBlogActivity.startActivityForTransition(requireActivity(), mViewBinding.llSearch)
        }
    }

    private fun setTopTabAndViewPager(){
        fragmentList.add(BlogListFragment())
        fragmentList.add(RankingListFragment())
        val myMainPagerAdapter = MyMainPagerAdapter(childFragmentManager,fragmentList)
        mViewBinding.topViewPager.adapter = myMainPagerAdapter
        mViewBinding.topViewPager.offscreenPageLimit = fragmentList.size
        mViewBinding.topViewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(mViewBinding.topTabLayout))
        mViewBinding.topTabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab) {
                val position = tab.position
                LogUtil.i("topTab选择了$position")
                mViewBinding.topViewPager.setCurrentItem(position,false)
                val customView = tab.customView
                if (customView != null){
                    val tvName = customView.findViewById<TextView>(R.id.tv_name)
                    tvName.setTextColor(resources.getColor(R.color.common_theme))
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                val customView = tab.customView
                if (customView != null) {
                    val tvName = customView.findViewById<TextView>(R.id.tv_name)
                    tvName.setTextColor(resources.getColor(R.color.common_text_unselect))
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })
        for (i in titles.indices) {
            val tab: TabLayout.Tab = mViewBinding.topTabLayout.newTab()
            val customView: View = LayoutInflater.from(context).inflate(R.layout.tab_top_item, null)
            tab.customView = customView
            val tvName = customView.findViewById<TextView>(R.id.tv_name)
            tvName.text = titles[i]
            if (0 == i) {
                tvName.setTextColor(resources.getColor(R.color.common_theme))
            }  else if (1 == i) {
                tvName.setTextColor(resources.getColor(R.color.common_text_unselect))
            }
            mViewBinding.topTabLayout.addTab(tab)
        }
    }

}