package com.zhq.zhq_blog.module.blog.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.zhq.zhq_blog.R
import com.zhq.zhq_blog.module.blog.bean.BlogPageListResult
import com.zhq.zhq_blog.util.GlideUtil

class BlogAdapter(data: MutableList<BlogPageListResult.Data>?) : BaseQuickAdapter<BlogPageListResult.Data, BaseViewHolder>(R.layout.rv_item_blog, data) {

    override fun convert(holder: BaseViewHolder, item: BlogPageListResult.Data) {
        holder.setText(R.id.tv_title, item.title)
        holder.setText(R.id.tv_content, item.content)
        holder.setText(R.id.tv_username, item.user.username)
        holder.setText(R.id.tv_date, item.createTime)
        GlideUtil.loadHead(context, item.user.iconUrl, holder.getView(R.id.iv_icon))
        val iv_collect = holder.getView<ImageView>(R.id.iv_collect)

        if (item.collectCount == 0){
            holder.getView<TextView>(R.id.tv_collect_count).visibility = View.INVISIBLE
        }else{
            holder.getView<TextView>(R.id.tv_collect_count).visibility = View.VISIBLE
            holder.setText(R.id.tv_collect_count, item.collectCount.toString())
        }
        holder.setText(R.id.tv_praise_count,item.praiseCount.toString())
        val iv_praise = holder.getView<ImageView>(R.id.iv_praise)
        if (item.praiseCount == 0){
            holder.getView<TextView>(R.id.tv_praise_count).visibility = View.INVISIBLE
        }else{
            holder.getView<TextView>(R.id.tv_praise_count).visibility = View.VISIBLE
            holder.setText(R.id.tv_praise_count, item.praiseCount.toString())
        }
        if (item.isMyCollected == 0) {
            iv_collect.setImageResource(R.drawable.ic_uncollect)
        } else {
            iv_collect.setImageResource(R.drawable.ic_collected)
        }
        if (item.isMyPraised == 0){
            iv_praise.setImageResource(R.drawable.ic_unpraise)
        } else {
            iv_praise.setImageResource(R.drawable.ic_praised)
        }
    }
}