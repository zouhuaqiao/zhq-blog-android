package com.zhq.zhq_blog.module.blog.viewmodel

import androidx.lifecycle.MutableLiveData
import com.jeremyliao.liveeventbus.LiveEventBus
import com.zhq.zhq_blog.module.blog.bean.BlogPageListResult
import com.zhq.zhq_blog.module.blog.bean.EditBlogRequest
import com.zhq.zhq_blog.module.blog.repository.BlogRepository
import com.zhq.zhq_blog.module.bus.UpdateBlogEvent
import com.zhq.zhq_blog.net.common.CommonResultBean
import com.zhq.zhq_blog.util.zhq.toast
import com.zhq.zhq_blog.base.BaseViewModel

/**
 *
 */
class EditBlogViewModel : BaseViewModel() {
    private val repository by lazy { BlogRepository() }

    fun getDetail(blogId: Int): MutableLiveData<CommonResultBean<BlogPageListResult.Data>> {
        val resultLiveData = MutableLiveData<CommonResultBean<BlogPageListResult.Data>>()
        launch(
            workBlock = {
                val result = repository.getById(blogId)
                handleResult(result,
                    successBlock = {
                        resultLiveData.value = result
                    },
                    failBlock = {
                        toast(result.msg)
                    }

                )
                resultLiveData.value = result //如果所有ui操作都再vm中处理完。是可以不传给activity结果的

            }, true, "正在获取详情..."
        )
        return resultLiveData
    }

    fun editBlog(request: EditBlogRequest): MutableLiveData<CommonResultBean<*>> {
        val resultLiveData = MutableLiveData<CommonResultBean<*>>()
        launch(
            workBlock = {
                val result = repository.updateBlog(request)
                handleResult(result,
                    successBlock = {
                        toast("修改成功")

                        UpdateBlogEvent().apply {
                            LiveEventBus.get(UpdateBlogEvent::class.java).post(this)
                        }


                    },
                    failBlock = {
                        toast(result.msg)
                    }

                )
                resultLiveData.value = result //如果所有ui操作都再vm中处理完。是可以不传给activity结果的

            }, true, "正在修改..."
        )
        return resultLiveData
    }

    fun deleteBlog(blogId: Int): MutableLiveData<CommonResultBean<*>> {
        val resultLiveData = MutableLiveData<CommonResultBean<*>>()
        launch(
            workBlock = {
                val result = repository.deleteBlog(blogId)
                handleResult(result,
                    successBlock = {
                        toast("删除成功")

                        UpdateBlogEvent().apply {
                            LiveEventBus.get(UpdateBlogEvent::class.java).post(this)
                        }


                    },
                    failBlock = {
                        toast(result.msg)
                    }

                )
                resultLiveData.value = result //如果所有ui操作都再vm中处理完。是可以不传给activity结果的

            }, true, "正在删除..."
        )
        return resultLiveData
    }


}