package com.zhq.zhq_blog.module.blog.bean;

import java.util.List;

/**
 * 一个博客的所有评论数据和回复数据
 */

public class CommentBeanResult {
    private int code;
    private String msg;
    private Data data;
    public void setCode(int code) {
        this.code = code;
    }
    public int getCode() {
        return code;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    public String getMsg() {
        return msg;
    }

    public void setData(Data data) {
        this.data = data;
    }
    public Data getData() {
        return data;
    }

    public class Data {

        private int total;
        private List<CommentDetailBean> list;
        public void setTotal(int total) {
            this.total = total;
        }
        public int getTotal() {
            return total;
        }

        public void setList(List<CommentDetailBean> list) {
            this.list = list;
        }
        public List<CommentDetailBean> getList() {
            return list;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "total=" + total +
                    ", list=" + list +
                    '}'+'\n';
        }
    }

    @Override
    public String toString() {
        return "CommentBeanResult{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                ", data=" + data +
                '}';
    }
}
