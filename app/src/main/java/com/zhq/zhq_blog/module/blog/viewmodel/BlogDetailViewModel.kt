package com.zhq.zhq_blog.module.blog.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.jeremyliao.liveeventbus.LiveEventBus
import com.zhq.zhq_blog.module.blog.repository.BlogRepository
import com.zhq.zhq_blog.net.common.CommonResultBean
import com.zhq.zhq_blog.util.zhq.toast
import com.zhq.zhq_blog.base.BaseViewModel
import com.zhq.zhq_blog.module.blog.bean.*
import com.zhq.zhq_blog.module.bus.UpdateBlogEvent

/**
 *
 */
class BlogDetailViewModel : BaseViewModel() {
    private val repository by lazy { BlogRepository() }

    fun getDetail(blogId: Int): MutableLiveData<CommonResultBean<BlogPageListResult.Data>> {
        val resultLiveData = MutableLiveData<CommonResultBean<BlogPageListResult.Data>>()
        launch(
            workBlock = {
                val result = repository.getById(blogId)
                Log.d("BlogDetailViewModel", "getDetail: result=$result")
                handleResult(result,
                    successBlock = {
                        resultLiveData.value = result
                    },
                    failBlock = {
                        toast(result.msg)
                    }

                )
                resultLiveData.value = result //如果所有ui操作都再vm中处理完。是可以不传给activity结果的

            }, true, "正在获取详情..."
        )
        return resultLiveData
    }

    fun getCommentDetail(blogId: Int): MutableLiveData<CommonResultBean<*>> {
        val resultLiveData = MutableLiveData<CommonResultBean<*>>()
        launch(
            workBlock = {
                val result = repository.getCommentList(blogId)
                Log.d("BlogDetailViewModel", "getCommentDetail: result=$result")
                handleResult(result,
                    successBlock = {
                        resultLiveData.value = result
                    },
                    failBlock = {
                        toast(result.msg)
                    }

                )
                resultLiveData.value = result

            }, true, "正在获取详情..."
        )
        return resultLiveData
    }

    fun addComment(addCommentRequest: AddCommentRequest): MutableLiveData<CommonResultBean<Int>> {
        val resultLiveData = MutableLiveData<CommonResultBean<Int>>()
        launch(
            workBlock = {
                val result = repository.addComment(addCommentRequest)
                handleResult(result,
                    successBlock = {
                        toast("评论成功")

                        UpdateBlogEvent().apply {
                            LiveEventBus.get(UpdateBlogEvent::class.java).post(this)
                        }


                    },
                    failBlock = {
                        toast(result.msg)
                    }

                )
                resultLiveData.value = result //如果所有ui操作都再vm中处理完。是可以不传给activity结果的

            }, true, "正在评论..."
        )
        return resultLiveData

    }

    fun addCommentReply(addCommentReplyRequest: AddCommentReplyRequest): MutableLiveData<CommonResultBean<Int>> {
        val resultLiveData = MutableLiveData<CommonResultBean<Int>>()
        launch(
            workBlock = {
                val result = repository.addCommentReply(addCommentReplyRequest)
                handleResult(result,
                    successBlock = {
                        toast("回复成功")

                        UpdateBlogEvent().apply {
                            LiveEventBus.get(UpdateBlogEvent::class.java).post(this)
                        }


                    },
                    failBlock = {
                        toast(result.msg)
                    }

                )
                resultLiveData.value = result //如果所有ui操作都再vm中处理完。是可以不传给activity结果的

            }, true, "正在回复..."
        )
        return resultLiveData
    }

    fun collect(blogId: Int, type: Int): MutableLiveData<CommonResultBean<*>> {
        val resultLiveData = MutableLiveData<CommonResultBean<*>>()
        launch(
            workBlock = {
                val result = repository.collect(blogId, type)
                resultLiveData.value = result //因为列表的ui操作太复杂，所以这里不处理了，返给view层

            }, false
        )
        return resultLiveData
    }

    fun praise(blogId: Int,type: Int): MutableLiveData<CommonResultBean<*>>{
        val resultLiveData = MutableLiveData<CommonResultBean<*>>()
        launch(
            workBlock = {
                val result = repository.praise(blogId, type)
                resultLiveData.value = result
            }, false
        )
        return resultLiveData
    }

}