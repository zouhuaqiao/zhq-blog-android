package com.zhq.zhq_blog.module.mian.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

/**
 *
 */
public class MyMainPagerAdapter extends FragmentPagerAdapter {
    private List<Fragment> fragmentList;

    public MyMainPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public MyMainPagerAdapter(FragmentManager fm, List<Fragment> fragmentList) {
        super(fm);
        this.fragmentList = fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList == null ? 0 : fragmentList.size();
    }
}
