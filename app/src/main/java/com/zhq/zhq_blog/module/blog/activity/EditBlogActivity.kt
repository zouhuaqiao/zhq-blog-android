package com.zhq.zhq_blog.module.blog.activity

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import com.zhq.zhq_blog.R
import com.zhq.zhq_blog.base.BaseIntentBean
import com.zhq.zhq_blog.base.BaseVMActivity
import com.zhq.zhq_blog.base.Constant
import com.zhq.zhq_blog.databinding.ActivityEditBlogBinding
import com.zhq.zhq_blog.module.blog.bean.EditBlogRequest
import com.zhq.zhq_blog.module.blog.markdown.EditorAction
import com.zhq.zhq_blog.module.blog.viewmodel.EditBlogViewModel
import com.zhq.zhq_blog.module.mian.activity.MainActivity
import com.zhq.zhq_blog.util.zhq.toast

class EditBlogActivity : BaseVMActivity<EditBlogViewModel>(), View.OnClickListener {
    val mViewBinding by lazy { ActivityEditBlogBinding.inflate(layoutInflater) }
    var intentBean: IntentBean = IntentBean(0)

    private lateinit var editorAction: EditorAction

    companion object {
        fun startActivity(context: Context, intentBean: IntentBean) {
            val intent = Intent(context, EditBlogActivity::class.java)
            intent.putExtra(Constant.IntentKey.IntentBean, intentBean)
            context.startActivity(intent)
        }

        data class IntentBean(var blogId: Int) : BaseIntentBean()
    }

    private fun initView() {
        mViewBinding.layoutTitle.ivBack.setOnClickListener {
            finishAfterTransition()
        }
        mViewBinding.layoutTitle.tvTitle.text = "编辑博客"
        editorAction = EditorAction(context, mViewBinding.etContent)
        mViewBinding.etContent.requestFocus()
        setOnClickListener()

    }

    override fun setContentView() {
        setContentView(mViewBinding.root)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        parseIntent<IntentBean>()?.apply {
            intentBean = this
        }
        initView()
        mViewBinding.tvEdit.setOnClickListener {
            val title = mViewBinding.etTitle.text.toString()
            val content = mViewBinding.etContent.text.toString()
            when {
                title.isEmpty() -> toast("标题不能为空")
                content.isEmpty() -> toast("内容不能为空")
                else -> {
                    netEditBlog(title, content)
                }
            }
        }
        mViewBinding.tvDelete.setOnClickListener {
            netDeleteBlog()
        }
        netGetDetail()
    }

    private fun netGetDetail() {
        mViewModel.getDetail(intentBean.blogId).observe(this) {
            if (it.isServerResultOK()) {
                it.data?.let {
                    mViewBinding.etTitle.setText(it.title)
                    mViewBinding.etContent.setText(it.content)
                }

            }
        }
    }

    fun setOnClickListener() {
        mViewBinding.editorActionbar.heading.setOnClickListener(this)
        mViewBinding.editorActionbar.bold.setOnClickListener(this)
        mViewBinding.editorActionbar.italic.setOnClickListener(this)
        mViewBinding.editorActionbar.code.setOnClickListener(this)
        mViewBinding.editorActionbar.quote.setOnClickListener(this)
        mViewBinding.editorActionbar.listNumber.setOnClickListener(this)
        mViewBinding.editorActionbar.listBullet.setOnClickListener(this)
        mViewBinding.editorActionbar.link.setOnClickListener(this)
        mViewBinding.editorActionbar.image.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        if (v != null) {
            when (v.id) {
                mViewBinding.editorActionbar.heading.id -> editorAction.heading()
                mViewBinding.editorActionbar.bold.id -> editorAction.bold()
                mViewBinding.editorActionbar.italic.id -> editorAction.italic()
                mViewBinding.editorActionbar.code.id -> editorAction.insertCode()
                mViewBinding.editorActionbar.quote.id -> editorAction.quote()
                mViewBinding.editorActionbar.listNumber.id -> editorAction.orderedList()
                mViewBinding.editorActionbar.listBullet.id -> editorAction.unorderedList()
                mViewBinding.editorActionbar.link.id -> editorAction.insertLink()
                mViewBinding.editorActionbar.image.id -> handleImage()
            }
        }
    }

    private fun handleImage() {
        //insert image
        val inputDialog: AlertDialog.Builder = AlertDialog.Builder(context)
        inputDialog.setTitle(R.string.dialog_title_insert_image)
        val inflater: LayoutInflater = layoutInflater
        val dialogView: View = inflater.inflate(R.layout.dialog_insert_image, null)
        inputDialog.setView(dialogView)
        inputDialog.setNegativeButton(
            R.string.cancel
        ) { dialog, which -> dialog.cancel() }

        inputDialog.setPositiveButton(
            R.string.dialog_btn_insert
        ) { dialog, which ->
            val imageDisplayText = dialogView.findViewById<EditText>(
                R.id.image_display_text
            )
            val imageUri = dialogView.findViewById<EditText>(R.id.image_uri)
            editorAction.insertImage(
                imageDisplayText.text.toString(),
                imageUri.text.toString()
            )
        }
        inputDialog.show()
    }

    private fun netEditBlog(title: String, content: String) {
        val request = EditBlogRequest()
        request.title = title
        request.content = content
        request.id = intentBean.blogId
        mViewModel.editBlog(request).observe(this) {
            if (it.isServerResultOK()) {
                finish()
            }
        }
    }

    private fun netDeleteBlog() {
        startActivity(Intent(activity, MainActivity::class.java))
        mViewModel.deleteBlog(intentBean.blogId).observe(this) {
            if (it.isServerResultOK()) {
                finish()
            }
        }
    }

}