package com.zhq.zhq_blog.module.user.viewmodel

import androidx.lifecycle.MutableLiveData
import com.jeremyliao.liveeventbus.LiveEventBus
import com.zhq.zhq_blog.module.bus.LoginStatusEvent
import com.zhq.zhq_blog.module.user.bean.LoginResult
import com.zhq.zhq_blog.net.common.CommonResultBean
import com.zhq.zhq_blog.store.UserManager
import com.zhq.zhq_blog.util.zhq.toast
import com.zhq.zhq_blog.base.BaseViewModel
import com.zhq.zhq_blog.module.user.repository.UserRepository

/**
 *
 */
class LoginViewModel : BaseViewModel() {
    private val userRepository by lazy { UserRepository() }

    //这里基本只做一些和ui相关的基本操作，比如toast，loading等，更具体的ui操作还是要到activity中
    fun login(phone: String, password: String): MutableLiveData<CommonResultBean<LoginResult>> {
        val resultLiveData = MutableLiveData<CommonResultBean<LoginResult>>()
        launch(
            workBlock = {
                val result = userRepository.login(phone, password)
                handleResult(result,
                    successBlock = {
                        toast("登录成功")
                        result.data?.let { data ->
                            val userInfo = UserManager.getUserInfo()
                            userInfo.isLogin = true
                            userInfo.phone = data.phone
                            userInfo.username = data.username
                            userInfo.icon = data.iconUrl
                            userInfo.token = data.token
                            userInfo.id = data.id
                            UserManager.updateUserInfo(userInfo)
                        }

                        LoginStatusEvent().apply {
                            isLogin = true
                            LiveEventBus.get(LoginStatusEvent::class.java).post(this)
                        }
                        resultLiveData.value = result //如果所有ui操作都再vm中处理完。是可以不传给activity结果的


                    },
                    failBlock = {
                        toast(result.msg)
                    }

                )

            }, true, "正在登录..."
        )
        return resultLiveData
    }

}