package com.zhq.zhq_blog.module.blog.bean

/**
 *
 */
data class BlogPageListResult(
    var currentPage: Int = 0, // 0
    var dataList: List<Data> = listOf(),
    var pageSize: Int = 0, // 0
    var total: Int = 0 // 0
) {
    data class Data(
        var content: String = "", // string
        var createTime: String = "", // 2022-01-17T09:49:19.393Z
        var id: Int = 0, // 0
        var title: String = "", // string
        var updateTime: String = "", // 2022-01-17T09:49:19.393Z
        var user: User = User(),
        var collectCount: Int = 0,
        var isMyCollected: Int = 0,
        var praiseCount: Int = 0,
        var isMyPraised: Int = 0,
        var userId: Int = 0 // 0
    ) {
        data class User(
            var iconUrl: String = "", // string
            var id: Int = 0, // 0
            var password: String = "", // string
            var phone: String = "", // string
            var registerTime: String = "", // 2022-01-17T09:49:19.393Z
            var token: String = "", // string
            var updateTime: String = "", // 2022-01-17T09:49:19.393Z
            var username: String = "" // string
        )
    }
}