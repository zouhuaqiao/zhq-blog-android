package com.zhq.zhq_blog.module.splash.activity

import android.animation.Animator
import android.os.Bundle
import android.view.View
import com.zhq.zhq_blog.store.AppManager
import com.zhq.zhq_blog.databinding.ActivitySplashBinding
import com.zhq.zhq_blog.base.BaseActivity
import kotlin.random.Random

/**
 *
 */
class SplashActivity : BaseActivity() {
    val mViewBinding by lazy { ActivitySplashBinding.inflate(layoutInflater) }
    override fun setContentView() {
        setContentView(mViewBinding.root)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewBinding.apply {
            animationView2.loop(false)

            btnGo.setOnClickListener {
                startAnimal2()
            }
            animationView2.addAnimatorListener(object : Animator.AnimatorListener {
                /**
                 *
                 * Notifies the start of the animation.
                 *
                 * @param animation The started animation.
                 */
                override fun onAnimationStart(animation: Animator?) {

                }

                /**
                 *
                 * Notifies the end of the animation. This callback is not invoked
                 * for animations with repeat count set to INFINITE.
                 *
                 * @param animation The animation which reached its end.
                 */
                override fun onAnimationEnd(animation: Animator?) {
                    AppManager.saveSplashShowedStatus(true)
                    finish()
                    AppManager.analyseGoToMain(this@SplashActivity)
                }

                /**
                 *
                 * Notifies the cancellation of the animation. This callback is not invoked
                 * for animations with repeat count set to INFINITE.
                 *
                 * @param animation The animation which was canceled.
                 */
                override fun onAnimationCancel(animation: Animator?) {

                }

                /**
                 *
                 * Notifies the repetition of the animation.
                 *
                 * @param animation The animation which was repeated.
                 */
                override fun onAnimationRepeat(animation: Animator?) {

                }

            })

            if (AppManager.getSplashShowedStatus()) {
                //展示过一次。就简化闪屏
                //随机展示闪屏
                val random = Random.nextInt(5)
                if (random == 1) {
                    startAnimal2()
                }else{
                    AppManager.analyseGoToMain(this@SplashActivity)
                    finish()
                }
            }
        }
    }

    private fun startAnimal2() {
        mViewBinding.apply {
            animationView.visibility = View.GONE
            animationView2.visibility = View.VISIBLE
            animationView2.playAnimation()
            btnGo.visibility = View.GONE
        }

    }

}