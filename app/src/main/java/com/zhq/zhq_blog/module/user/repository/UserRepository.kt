package com.zhq.zhq_blog.module.user.repository

import com.zhq.zhq_blog.net.common.MainRetrofitManager
import com.zhq.zhq_blog.base.BaseRepository

/**
 *
 */
class UserRepository : BaseRepository() {
    suspend fun login(phone: String, password: String) =
        safeGetData { MainRetrofitManager.apiService.login(phone, password) }

    suspend fun register(phone: String, password: String) =
        safeGetData { MainRetrofitManager.apiService.register(phone, password) }

    suspend fun userInfo() =
        safeGetData { MainRetrofitManager.apiService.userInfo() }

    suspend fun changeIcon(iconUrl: String) =
        safeGetData { MainRetrofitManager.apiService.iconUrl(iconUrl) }

    suspend fun changeUsername(username: String) =
        safeGetData { MainRetrofitManager.apiService.changeUsername(username) }

    suspend fun changePassword(password: String) =
        safeGetData { MainRetrofitManager.apiService.changePassword(password) }

}