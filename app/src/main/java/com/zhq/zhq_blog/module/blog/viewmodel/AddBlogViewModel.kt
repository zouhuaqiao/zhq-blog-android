package com.zhq.zhq_blog.module.blog.viewmodel

import androidx.lifecycle.MutableLiveData
import com.jeremyliao.liveeventbus.LiveEventBus
import com.zhq.zhq_blog.module.blog.bean.AddBlogRequest
import com.zhq.zhq_blog.module.blog.repository.BlogRepository
import com.zhq.zhq_blog.module.bus.UpdateBlogEvent
import com.zhq.zhq_blog.net.common.CommonResultBean
import com.zhq.zhq_blog.util.zhq.toast
import com.zhq.zhq_blog.base.BaseViewModel

/**
 *
 */
class AddBlogViewModel : BaseViewModel() {
    private val repository by lazy { BlogRepository() }

    fun add(title: String, content: String): MutableLiveData<CommonResultBean<Int>> {
        val addBlogRequest = AddBlogRequest(content,title)
        val resultLiveData = MutableLiveData<CommonResultBean<Int>>()
        launch(
            workBlock = {
                var result = repository.add(addBlogRequest)
                handleResult(result,
                    successBlock = {
                        toast("发布成功")

                        UpdateBlogEvent().apply {
                            LiveEventBus.get(UpdateBlogEvent::class.java).post(this)
                        }


                    },
                    failBlock = {
                        toast(result.msg)
                    }

                )
                resultLiveData.value = result //如果所有ui操作都再vm中处理完。是可以不传给activity结果的

            }, true, "正在发布..."
        )
        return resultLiveData
    }


}