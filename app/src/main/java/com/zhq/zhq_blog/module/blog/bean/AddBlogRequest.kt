package com.zhq.zhq_blog.module.blog.bean

/**
 *
 */
data class AddBlogRequest(
    var content: String = "", // string
    var title: String = "" // string
)