package com.zhq.zhq_blog.module.setting.activity

import android.os.Bundle
import com.zhq.zhq_blog.R
import com.zhq.zhq_blog.base.BaseActivity
import com.zhq.zhq_blog.databinding.ActivitySettingBinding
import com.zhq.zhq_blog.module.setting.fragment.SettingsFragment

class SettingActivity : BaseActivity() {
    val mViewBinding by lazy { ActivitySettingBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_container, SettingsFragment())
            .commit()

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun setContentView() {
        setContentView(mViewBinding.root)
    }
}