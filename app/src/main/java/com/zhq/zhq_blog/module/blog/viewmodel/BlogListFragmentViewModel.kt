package com.zhq.zhq_blog.module.blog.viewmodel

import androidx.lifecycle.MutableLiveData
import com.zhq.zhq_blog.module.blog.bean.BlogPageListResult
import com.zhq.zhq_blog.module.blog.repository.BlogRepository
import com.zhq.zhq_blog.net.common.CommonResultBean
import com.zhq.zhq_blog.util.PageBean
import com.zhq.zhq_blog.base.BaseViewModel

/**
 *
 */
class BlogListFragmentViewModel : BaseViewModel() {
    private val repository by lazy { BlogRepository() }
    val livedataComplete = MutableLiveData<Any>()

    fun getPageList(page: PageBean): MutableLiveData<CommonResultBean<BlogPageListResult>> {
        val resultLiveData = MutableLiveData<CommonResultBean<BlogPageListResult>>()
        launch(
            workBlock = {
                val result = repository.getPageList(page)
                livedataComplete.value = true
                resultLiveData.value = result //因为列表的ui操作太复杂，所以这里不处理了，返给view层

            }, false
        )
        return resultLiveData
    }

    fun collect(blogId: Int, type: Int): MutableLiveData<CommonResultBean<*>> {
        val resultLiveData = MutableLiveData<CommonResultBean<*>>()
        launch(
            workBlock = {
                val result = repository.collect(blogId, type)
                resultLiveData.value = result //因为列表的ui操作太复杂，所以这里不处理了，返给view层

            }, false
        )
        return resultLiveData
    }

    fun praise(blogId: Int,type: Int): MutableLiveData<CommonResultBean<*>>{
        val resultLiveData = MutableLiveData<CommonResultBean<*>>()
        launch(
            workBlock = {
                val result = repository.praise(blogId, type)
                resultLiveData.value = result
            }, false
        )
        return resultLiveData
    }


}