package com.zhq.zhq_blog.module.blog.viewmodel

import androidx.lifecycle.MutableLiveData
import com.zhq.zhq_blog.module.blog.bean.BlogPageListResult
import com.zhq.zhq_blog.module.blog.repository.BlogRepository
import com.zhq.zhq_blog.net.common.CommonResultBean
import com.zhq.zhq_blog.util.PageBean
import com.zhq.zhq_blog.base.BaseViewModel

/**
 *
 */
class SearchBlogViewModel : BaseViewModel() {
    private val repository by lazy { BlogRepository() }
    val livedataComplete = MutableLiveData<Any>()

    fun searchBlog(page: PageBean, keywords: String): MutableLiveData<CommonResultBean<BlogPageListResult>> {
        val resultLiveData = MutableLiveData<CommonResultBean<BlogPageListResult>>()
        launch(
            workBlock = {
                val result = repository.searchBlog(page,keywords)
                livedataComplete.value = true
                resultLiveData.value = result //因为列表的ui操作太复杂，所以这里不处理了，返给view层

            }, false
        )
        return resultLiveData
    }

}