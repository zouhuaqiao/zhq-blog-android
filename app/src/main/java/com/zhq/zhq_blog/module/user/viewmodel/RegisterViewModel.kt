package com.zhq.zhq_blog.module.user.viewmodel

import androidx.lifecycle.MutableLiveData
import com.zhq.zhq_blog.net.common.CommonResultBean
import com.zhq.zhq_blog.util.zhq.toast
import com.zhq.zhq_blog.base.BaseViewModel
import com.zhq.zhq_blog.module.blog.repository.BlogRepository

/**
 *
 */
class RegisterViewModel : BaseViewModel() {
    private val userRepository by lazy { BlogRepository() }

    //这里基本只做一些和ui相关的基本操作，比如toast，loading等，更具体的ui操作还是要到activity中
    fun register(phone: String, password: String): MutableLiveData<CommonResultBean<*>> {
        val resultLiveData = MutableLiveData<CommonResultBean<*>>()
        launch(
            workBlock = {
                val result = userRepository.register(phone, password)
                handleResult(result,
                    successBlock = {
                        toast("注册成功")
                        resultLiveData.value = result //如果所有ui操作都再vm中处理完。是可以不传给activity结果的
                    },
                    failBlock = {
                        toast(result.msg)
                    }
                )
            }, true, "正在注册..."
        )
        return resultLiveData
    }

}