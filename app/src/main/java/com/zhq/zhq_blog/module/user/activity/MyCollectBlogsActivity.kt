package com.zhq.zhq_blog.module.user.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.jeremyliao.liveeventbus.LiveEventBus
import com.kingja.loadsir.core.LoadService
import com.kingja.loadsir.core.LoadSir
import com.zhq.zhq_blog.R
import com.zhq.zhq_blog.base.BaseVMActivity
import com.zhq.zhq_blog.callback.LoadingCallback
import com.zhq.zhq_blog.databinding.ActivityMyCollectedBlogsBinding
import com.zhq.zhq_blog.module.blog.activity.BlogDetailActivity
import com.zhq.zhq_blog.module.blog.viewmodel.MyCollectedBlogsViewModel
import com.zhq.zhq_blog.module.bus.UpdateBlogEvent
import com.zhq.zhq_blog.util.CommonLinearItemDecoration
import com.zhq.zhq_blog.util.PageUtil
import com.zhq.zhq_blog.util.RefreshUtil
import com.zhq.zhq_blog.util.zhq.ZHQUtil
import com.zhq.zhq_blog.util.zhq.toast
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener
import com.zhq.zhq_blog.module.blog.adapter.BlogAdapter

class MyCollectBlogsActivity : BaseVMActivity<MyCollectedBlogsViewModel>() {
    val mViewBinding by lazy { ActivityMyCollectedBlogsBinding.inflate(layoutInflater) }
    val adapter: BlogAdapter by lazy { BlogAdapter(null) }
    private lateinit var loadService: LoadService<Any>

    companion object {
        fun startActivity(context: Context) {
            val intent = Intent(context, MyCollectBlogsActivity::class.java)
            context.startActivity(intent)
        }
    }


    override fun setContentView() {
        setContentView(mViewBinding.root)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTitle()
        loadService = LoadSir.getDefault().register(
            mViewBinding.smartRefreshLayout
        ) {
            loadService.showCallback(LoadingCallback::class.java)
            netQueryCollects(true)
        }
        loadService.showCallback(LoadingCallback::class.java)
        mViewBinding.recyclerView.layoutManager = LinearLayoutManager(activity)
        mViewBinding.recyclerView.addItemDecoration(
            CommonLinearItemDecoration(
                dividerColor = context.getColor(R.color.common_theme),
                dividerHeight = ZHQUtil.dp2px(0.5f)
            )
        )
        mViewBinding.recyclerView.adapter = adapter
        adapter.setOnItemClickListener { _, _, position ->
            BlogDetailActivity.startActivity(
                activity,
                intentBean = BlogDetailActivity.Companion.IntentBean(this.adapter.getItem(position).id)
            )
        }
        //这里就不写收藏，取消收藏的逻辑了。麻烦。首页写过了
        mViewBinding.smartRefreshLayout.setEnableLoadMore(true)
        mViewBinding.smartRefreshLayout.setOnRefreshLoadMoreListener(object :
            OnRefreshLoadMoreListener {
            override fun onRefresh(refreshLayout: RefreshLayout) {
                netQueryCollects(true)
            }

            override fun onLoadMore(refreshLayout: RefreshLayout) {
                netQueryCollects(false)
            }
        })
        initBus()
        netQueryCollects(true)
    }

    private fun initBus() {
        //监听事件总线的消息
        LiveEventBus
            .get(UpdateBlogEvent::class.java)
            .observe(this) {
                netQueryCollects(true)
            }
    }

    private fun initTitle() {
        mViewBinding.layoutTitle.ivBack.setOnClickListener {
            finish()
        }
        mViewBinding.layoutTitle.tvTitle.text = "我的收藏"
    }

    override fun observeViewModel() {
        super.observeViewModel()
        mViewModel.livedataComplete.observe(this) {
            mViewBinding.smartRefreshLayout.finishRefresh()
            mViewBinding.smartRefreshLayout.finishLoadMore()
        }
    }

    fun netQueryCollects(isRefresh: Boolean) {
        if (isRefresh) {
            mViewBinding.smartRefreshLayout.setNoMoreData(false)
        }
        mViewModel.queryCollects(
            PageUtil.getNextServerPageBean(isRefresh, adapter.data.size)
        )
            .observe(this) {
                RefreshUtil.changeRefreshViewStatus(
                    mViewBinding.smartRefreshLayout,
                    it?.data?.dataList?.size ?: 0,
                )
                if (it.isServerResultOK()) {
                    if (isRefresh) {
                        adapter.setNewInstance(it.data?.dataList?.toMutableList())
                    } else {
                        adapter.addData(it.data?.dataList!!.toMutableList())
                    }
                } else {
                    toast(it.msg)
                }

                RefreshUtil.changeLoadServiceStatus(it.code, loadService, adapter.data.size)
            }

    }

}