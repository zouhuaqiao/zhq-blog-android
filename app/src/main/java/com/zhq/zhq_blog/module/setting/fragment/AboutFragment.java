package com.zhq.zhq_blog.module.setting.fragment;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.flipboard.bottomsheet.commons.MenuSheetView;
import com.zhq.zhq_blog.R;
import com.zhq.zhq_blog.base.BaseFragment;
import com.zhq.zhq_blog.base.Constant;
import com.zhq.zhq_blog.databinding.FragmentAboutBinding;

import java.util.Objects;

public class AboutFragment extends BaseFragment {
    private FragmentAboutBinding mViewBinding;
    protected AppCompatActivity context;
    protected Toolbar toolbar;
    protected String toolbarTitle;

    protected boolean setDisplayHomeAsUpEnabled = true;

    @NonNull
    @Override
    protected View createView(@Nullable LayoutInflater inflater, @Nullable ViewGroup container) {
        mViewBinding = FragmentAboutBinding.inflate(inflater,container,false);
        return mViewBinding.getRoot();
    }
    @Override
    protected void onLazyLoad() {

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        context = (AppCompatActivity) getContext();
        initView();
    }

    public void initView() {
        toolbarTitle = "关于";
        toolbar = mViewBinding.toolbar;
        toolbar.setTitle(toolbarTitle);
        context.setSupportActionBar(toolbar);
        if (setDisplayHomeAsUpEnabled) {
            Objects.requireNonNull(context.getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        }
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(
                    requireContext().getPackageName(), 0);
            String versionName = packageInfo.versionName;
            int versionCode = packageInfo.versionCode;
            mViewBinding.appVersionNumber.setText(versionName + " ( " + versionCode + " ) ");
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(getClass().getName(), e.getMessage());
            e.printStackTrace();
        }
        mViewBinding.projectPageBtn.setOnClickListener(v -> openUri(Constant.PROJECT_PAGE_URL));
        mViewBinding.contactBtn.setOnClickListener(v -> showMenuSheet(MenuSheetView.MenuType.GRID));
    }

    public void showMenuSheet(MenuSheetView.MenuType menuType) {
        MenuSheetView menuSheetView = new MenuSheetView(context, menuType, "选择通过什么联系我...",
                item -> {
                    switch (item.getItemId()) {
                        case R.id.email:
                            Intent intent = new Intent(Intent.ACTION_SENDTO);
                            intent.setData(Uri.parse("mailto:" + Constant.MY_EMAIL));
                            startActivity(intent);
                            break;
                        case R.id.github:
                            openUri(Constant.MY_GITHUB);
                            break;
                        case R.id.zhihu:
                            openUri(Constant.MY_ZHIHU);
                            break;
                    }
                    if (mViewBinding.bottomSheet.isSheetShowing()) {
                        mViewBinding.bottomSheet.dismissSheet();
                    }
                    return true;
                });
        menuSheetView.inflateMenu(R.menu.about_bottomsheet_menu);
        mViewBinding.bottomSheet.showWithSheetView(menuSheetView);
    }

    public void openUri(String uriString) {
        Uri uri = Uri.parse(uriString);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

}
