package com.zhq.zhq_blog.module.blog.bean;

/**
 * 回复数据
 */

public class ReplyDetailBean {
    private String nickName;
    private int id;
    private int commentId;
    private String content;
    private String status;
    private String createDate;

    public ReplyDetailBean(String content) {
        this.nickName = nickName;
        this.content = content;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
    public String getNickName() {
        return nickName;
    }

    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public void setContent(String content) {
        this.content = content;
    }
    public String getContent() {
        return content;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    public String getStatus() {
        return status;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }
    public String getCreateDate() {
        return createDate;
    }

    @Override
    public String toString() {
        return "ReplyDetailBean{" +
                "nickName='" + nickName + '\'' +
                ", id=" + id +
                ", commentId='" + commentId + '\'' +
                ", content='" + content + '\'' +
                ", status='" + status + '\'' +
                ", createDate='" + createDate + '\'' +
                '}'+'\n';
    }
}
