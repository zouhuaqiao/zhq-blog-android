package com.zhq.zhq_blog.module.user.viewmodel

import androidx.lifecycle.MutableLiveData
import com.zhq.zhq_blog.module.user.bean.LoginResult
import com.zhq.zhq_blog.net.common.CommonResultBean
import com.zhq.zhq_blog.store.UserManager
import com.zhq.zhq_blog.util.zhq.toast
import com.zhq.zhq_blog.base.BaseViewModel
import com.zhq.zhq_blog.module.user.repository.UserRepository

/**
 *
 */
class MyFragmentViewModel : BaseViewModel() {
    private val repository by lazy { UserRepository() }

    fun getUserInfo(): MutableLiveData<CommonResultBean<LoginResult>> {
        val resultLiveData = MutableLiveData<CommonResultBean<LoginResult>>()
        launch(
            workBlock = {
                val result = repository.userInfo()
                handleResult(result,
                    successBlock = {
                        result.data?.let { data ->
                            val userInfo = UserManager.getUserInfo()
                            userInfo.isLogin = true
                            userInfo.phone = data.phone
                            userInfo.username = data.username
                            userInfo.icon = data.iconUrl
                            userInfo.id = data.id
                            UserManager.updateUserInfo(userInfo)
                        }
                        resultLiveData.value = result //如果所有ui操作都再vm中处理完。是可以不传给activity结果的
                    },
                    failBlock = {
                        toast(result.msg)
                    }
                )
            }, false
        )
        return resultLiveData
    }

}