package com.zhq.zhq_blog.module.user.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.jeremyliao.liveeventbus.LiveEventBus
import com.kingja.loadsir.core.LoadService
import com.kingja.loadsir.core.LoadSir
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener
import com.zhq.zhq_blog.R
import com.zhq.zhq_blog.base.BaseVMActivity
import com.zhq.zhq_blog.callback.LoadingCallback
import com.zhq.zhq_blog.databinding.ActivityMyAddedBlogsBinding
import com.zhq.zhq_blog.module.blog.activity.BlogDetailActivity
import com.zhq.zhq_blog.module.blog.adapter.BlogAdapter
import com.zhq.zhq_blog.module.bus.UpdateBlogEvent
import com.zhq.zhq_blog.module.user.viewmodel.MyAddedBlogsViewModel
import com.zhq.zhq_blog.util.CommonLinearItemDecoration
import com.zhq.zhq_blog.util.PageUtil
import com.zhq.zhq_blog.util.RefreshUtil
import com.zhq.zhq_blog.util.zhq.ZHQUtil
import com.zhq.zhq_blog.util.zhq.toast

/**
 * 作用:我的发布
 */
class MyAddedBlogsActivity : BaseVMActivity<MyAddedBlogsViewModel>() {
    val mViewBinding by lazy { ActivityMyAddedBlogsBinding.inflate(layoutInflater) }
    val adapter: BlogAdapter by lazy { BlogAdapter(null) }
    private lateinit var loadService: LoadService<Any>

    companion object {
        fun startActivity(context: Context) {
            val intent = Intent(context, MyAddedBlogsActivity::class.java)
            context.startActivity(intent)
        }
    }


    override fun setContentView() {
        setContentView(mViewBinding.root)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTitle()
        loadService = LoadSir.getDefault().register(
            mViewBinding.smartRefreshLayout
        ) {
            loadService.showCallback(LoadingCallback::class.java)
            netQueryMyAddedBlogs(true)
        }
        loadService.showCallback(LoadingCallback::class.java)
        mViewBinding.recyclerView.layoutManager = LinearLayoutManager(activity)
        mViewBinding.recyclerView.addItemDecoration(
            CommonLinearItemDecoration(
                dividerColor = context.getColor(R.color.common_theme),
                dividerHeight = ZHQUtil.dp2px(0.5f)
            )
        )
        mViewBinding.recyclerView.adapter = adapter
        adapter.setOnItemClickListener { _, _, position ->
            BlogDetailActivity.startActivity(
                activity,
                intentBean = BlogDetailActivity.Companion.IntentBean(this.adapter.getItem(position).id)
            )
        }
        //这里就不写收藏，取消收藏的逻辑了。麻烦。首页写过了
        mViewBinding.smartRefreshLayout.setEnableLoadMore(true)
        mViewBinding.smartRefreshLayout.setOnRefreshLoadMoreListener(object :
            OnRefreshLoadMoreListener {
            override fun onRefresh(refreshLayout: RefreshLayout) {
                netQueryMyAddedBlogs(true)
            }

            override fun onLoadMore(refreshLayout: RefreshLayout) {
                netQueryMyAddedBlogs(false)
            }
        })
        initBus()
        netQueryMyAddedBlogs(true)
    }

    private fun initTitle() {
        mViewBinding.layoutTitle.ivBack.setOnClickListener {
            finish()
        }
        mViewBinding.layoutTitle.tvTitle.text = "我的发布"
    }

    private fun initBus() {
        //监听事件总线的消息
        LiveEventBus
            .get(UpdateBlogEvent::class.java)
            .observe(this) {
                netQueryMyAddedBlogs(true)
            }
    }

    override fun observeViewModel() {
        super.observeViewModel()
        mViewModel.livedataComplete.observe(this) {
            mViewBinding.smartRefreshLayout.finishRefresh()
            mViewBinding.smartRefreshLayout.finishLoadMore()
        }
    }

    fun netQueryMyAddedBlogs(isRefresh: Boolean) {
        if (isRefresh) {
            mViewBinding.smartRefreshLayout.setNoMoreData(false)
        }
        mViewModel.queryMyAddedBlogs(
            PageUtil.getNextServerPageBean(isRefresh, adapter.data.size)
        )
            .observe(this) {
                RefreshUtil.changeRefreshViewStatus(
                    mViewBinding.smartRefreshLayout,
                    it?.data?.dataList?.size ?: 0,
                )
                if (it.isServerResultOK()) {
                    if (isRefresh) {
                        adapter.setNewInstance(it.data?.dataList?.toMutableList())
                    } else {
                        adapter.addData(it.data?.dataList!!.toMutableList())
                    }
                } else {
                    toast(it.msg)
                }
                RefreshUtil.changeLoadServiceStatus(it.code, loadService, adapter.data.size)
            }
    }

}