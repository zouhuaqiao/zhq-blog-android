package com.zhq.zhq_blog.module.user.viewmodel

import androidx.lifecycle.MutableLiveData
import com.zhq.zhq_blog.net.common.CommonResultBean
import com.zhq.zhq_blog.base.BaseViewModel
import com.zhq.zhq_blog.module.user.repository.UserRepository

/**
 *
 */
class ChangeIconViewModel : BaseViewModel() {
    private val repository by lazy { UserRepository() }

    fun changeIcon(iconUrl: String): MutableLiveData<CommonResultBean<*>> {
        val resultLiveData = MutableLiveData<CommonResultBean<*>>()
        launch(
            workBlock = {
                val result = repository.changeIcon(iconUrl)
                resultLiveData.value = result //因为列表的ui操作太复杂，所以这里不处理了，返给view层

            }, true,"正在修改..."
        )
        return resultLiveData
    }


}