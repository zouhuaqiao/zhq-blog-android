package com.zhq.zhq_blog.module.user.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jeremyliao.liveeventbus.LiveEventBus
import com.zhq.zhq_blog.base.BaseVMFragment
import com.zhq.zhq_blog.databinding.FragmentMyBinding
import com.zhq.zhq_blog.module.blog.activity.HelpActivity
import com.zhq.zhq_blog.module.bus.UpdateUserInfoEvent
import com.zhq.zhq_blog.module.setting.activity.SettingActivity
import com.zhq.zhq_blog.module.user.activity.*
import com.zhq.zhq_blog.module.user.viewmodel.MyFragmentViewModel
import com.zhq.zhq_blog.store.UserManager
import com.zhq.zhq_blog.util.ActivityUtils
import com.zhq.zhq_blog.util.GlideUtil
import com.zhq.zhq_blog.util.zhq.toast

/**
 *
 */
class MyFragment : BaseVMFragment<MyFragmentViewModel>() {
    lateinit var mViewBinding: FragmentMyBinding
    override fun createView(inflater: LayoutInflater?, container: ViewGroup?): View {
        mViewBinding = FragmentMyBinding.inflate(inflater!!, container, false)
        return mViewBinding.root
    }

    override fun onLazyLoad() {
        netGetUserInfo()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewBinding.ivHead.setOnClickListener {
            ChangeIconActivity.startActivityForTransition(requireActivity(), mViewBinding.ivHead)
        }
        mViewBinding.tvUsername.setOnClickListener {
            ChangeUserNameActivity.startActivity(requireActivity())
        }
        mViewBinding.tvLogout.setOnClickListener {
            toast("已退出账号")
            UserManager.cleanUserInfo()
            ActivityUtils.screenManager.clearAllActivity()
            val intent = Intent(requireContext(), LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)

        }
        mViewBinding.mivPassword.setOnClickListener{
            ChangePasswordActivity.startActivity(requireContext())
        }
        mViewBinding.mivCollect.setOnClickListener {
            MyCollectBlogsActivity.startActivity(requireContext())
        }
        mViewBinding.mivMyAdded.setOnClickListener {
            MyAddedBlogsActivity.startActivity(requireContext())
        }
        mViewBinding.mivPraised.setOnClickListener{
            MyPraiseBlogsActivity.startActivity(requireContext())
        }
        mViewBinding.mivMyHelp.setOnClickListener{
            startActivity(Intent(context,HelpActivity::class.java))
        }
        mViewBinding.mivMySetting.setOnClickListener {
            startActivity(Intent(context,SettingActivity::class.java))
        }
        initBus()
    }

    private fun netGetUserInfo() {
        mViewModel.getUserInfo().observe(viewLifecycleOwner) {
            if (it.isServerResultOK()) {
                mViewBinding.tvUsername.text = it.data?.username
                GlideUtil.loadHead(activity, it.data?.iconUrl, mViewBinding.ivHead)
            }
        }
    }
    private fun initBus() {
        //监听事件总线的消息
        LiveEventBus
            .get(UpdateUserInfoEvent::class.java)
            .observe(this) {
                netGetUserInfo()
            }
    }
}