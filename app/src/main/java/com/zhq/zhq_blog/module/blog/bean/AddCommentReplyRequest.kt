package com.zhq.zhq_blog.module.blog.bean

data class AddCommentReplyRequest(
    var commentId:Int = 0,
    var content:String = "",
    var status:String = "01"
)