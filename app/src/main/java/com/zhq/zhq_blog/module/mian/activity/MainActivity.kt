package com.zhq.zhq_blog.module.mian.activity

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import com.gyf.immersionbar.ImmersionBar
import com.zhq.zhq_blog.R
import com.zhq.zhq_blog.base.BaseVMActivity
import com.zhq.zhq_blog.module.mian.adapter.MyMainPagerAdapter
import com.zhq.zhq_blog.module.user.fragment.MyFragment
import com.zhq.zhq_blog.util.log.LogUtil
import com.zhq.zhq_blog.util.zhq.toast
import com.zhq.zhq_blog.base.BaseViewModel
import com.zhq.zhq_blog.databinding.ActivityMainBinding
import com.zhq.zhq_blog.module.blog.fragment.TopTabFragment

class MainActivity : BaseVMActivity<BaseViewModel>() {
    var fragmentList: ArrayList<Fragment> = ArrayList()
    private var titles: List<String> = listOf("博客", "我的")
    val mViewBinding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    var animatorSet: AnimatorSet? = null
    var lastPressTime = 0L

    override fun setContentView() {
        setContentView(mViewBinding.root)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTabAndViewPager()
    }
    private fun setTabAndViewPager() {
        fragmentList.add(TopTabFragment())
        fragmentList.add(MyFragment())
        val myMainPagerAdapter = MyMainPagerAdapter(supportFragmentManager, fragmentList)
        mViewBinding.viewPager.adapter = myMainPagerAdapter
        mViewBinding.viewPager.offscreenPageLimit = fragmentList.size
        //Viewpager的监听（这个接听是为Tablayout专门设计的）
        mViewBinding.viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(mViewBinding.tabLayout))
        mViewBinding.tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                val position = tab.position
                LogUtil.i("tab选择了$position")
                mViewBinding.viewPager.setCurrentItem(position, false)
                val customView = tab.customView
                if (customView != null) {
                    val tvName: TextView = customView.findViewById<TextView>(R.id.tv_name)
                    val ivImage = customView.findViewById<ImageView>(R.id.iv_image)
                    tvName.setTextColor(resources.getColor(R.color.common_theme))
                    if (0 == position) {
                        ivImage.background = ResourcesCompat.getDrawable(resources,R.drawable.tab_home_select,theme)
                        startAnimator(ivImage)     //播放”博客“导航栏图标动画
                    } else if (1 == position) {
                        ivImage.background = ResourcesCompat.getDrawable(resources,R.drawable.tab_my_select,theme)
                        startAnimator(ivImage)     //播放“我的”导航栏图标动画
                    }
                }
                updateStatusBar()
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {
                val customView = tab.customView
                val position = tab.position
                if (customView != null) {
                    val tvName = customView.findViewById<TextView>(R.id.tv_name)
                    val ivImage = customView.findViewById<ImageView>(R.id.iv_image)
                    tvName.setTextColor(ResourcesCompat.getColor(resources,R.color.common_text_unselect,theme))
                    if (0 == position) {
                        ivImage.background = ResourcesCompat.getDrawable(resources,R.drawable.tab_home_unselect,theme)
                    }  else if (1 == position) {
                        ivImage.background = ResourcesCompat.getDrawable(resources,R.drawable.tab_my_unselect,theme)
                    }
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab) {}
        })
        for (i in titles.indices) {
            val tab: TabLayout.Tab = mViewBinding.tabLayout.newTab()
            val customView: View = LayoutInflater.from(this).inflate(R.layout.tab_main_item, null)
            tab.customView = customView
            val tvName = customView.findViewById<TextView>(R.id.tv_name)
            val ivImage = customView.findViewById<ImageView>(R.id.iv_image)
            tvName.text = titles[i]
            if (0 == i) {
                ivImage.background = ResourcesCompat.getDrawable(resources,R.drawable.tab_home_select,theme)
                tvName.setTextColor(ResourcesCompat.getColor(resources,R.color.common_theme,theme))
            }  else if (1 == i) {
                ivImage.background = ResourcesCompat.getDrawable(resources,R.drawable.tab_my_unselect,theme)
                tvName.setTextColor(ResourcesCompat.getColor(resources,R.color.common_text_unselect,theme))
            }
            mViewBinding.tabLayout.addTab(tab)
        }

    }

    //更新顶部状态栏
    private fun updateStatusBar() {

        if(mViewBinding.tabLayout.selectedTabPosition == 0){
            ImmersionBar.with(this) //状态栏颜色
                .statusBarColor(R.color.common_bg_white) //状态栏文字颜色
                .statusBarDarkFont(true)
                .fitsSystemWindows(true) //使用该属性必须指定状态栏的颜色，不然状态栏透明，很难看. false表示布局嵌入状态栏。true表示布局避开状态栏
                .init()
        }else if(mViewBinding.tabLayout.selectedTabPosition == 1){
            ImmersionBar.with(this) //状态栏颜色
                .transparentStatusBar() //状态栏文字颜色
                .statusBarDarkFont(true)
                .fitsSystemWindows(false) //使用该属性必须指定状态栏的颜色，不然状态栏透明，很难看. false表示布局嵌入状态栏。true表示布局避开状态栏
                .init()
        }

    }
    override fun onBackPressed() {
        val currentTimeMillis = System.currentTimeMillis()
        if (currentTimeMillis - lastPressTime > 2000) {
            toast("再按一次退出程序")
            lastPressTime = currentTimeMillis
        } else {
            super.onBackPressed()
        }
    }

    private fun startAnimator(view: View) {
        // 动画
        if (animatorSet != null) {
            //之前有别的动画，先结束掉,而不是取消.取消的话会停留在动画中的某个位置
//            animatorSet.cancel();
            animatorSet!!.end()
        }
        animatorSet = AnimatorSet()
        val duration: Long = 300
        animatorSet!!.playTogether(
            ObjectAnimator.ofFloat(view, "scaleX", 0.5.toFloat(), 1f)
                .setDuration(duration),
            ObjectAnimator.ofFloat(view, "scaleY", 0.5.toFloat(), 1f)
                .setDuration(duration),
            ObjectAnimator.ofFloat(view, "alpha", 0.5.toFloat(), 1f)
                .setDuration(duration),
            ObjectAnimator.ofFloat(view, "Rotation", 180f, 360f)
                .setDuration(duration)
        )
        animatorSet!!.start()
    }
}