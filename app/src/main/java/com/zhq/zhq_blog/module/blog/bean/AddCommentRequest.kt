package com.zhq.zhq_blog.module.blog.bean

data class AddCommentRequest(
    var blogId:Int = 0,
    var content:String = "",
    var imgId:String = "xcclsscrt0tev11ok364"
)