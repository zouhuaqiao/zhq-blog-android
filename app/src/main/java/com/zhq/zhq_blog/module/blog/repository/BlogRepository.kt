package com.zhq.zhq_blog.module.blog.repository

import com.zhq.zhq_blog.net.common.MainRetrofitManager
import com.zhq.zhq_blog.util.PageBean
import com.zhq.zhq_blog.base.BaseRepository
import com.zhq.zhq_blog.module.blog.bean.*

/**
 *
 */
class BlogRepository : BaseRepository() {
    suspend fun getPageList(page: PageBean) =
        safeGetData { MainRetrofitManager.apiService.blogPageList(page.pageNo, page.pageSize) }

    suspend fun searchBlog(page: PageBean, keywords: String) =
        safeGetData { MainRetrofitManager.apiService.searchBlog(page.pageNo, page.pageSize, keywords) }

    suspend fun queryCollects(page: PageBean) =
        safeGetData { MainRetrofitManager.apiService.queryCollects(page.pageNo, page.pageSize) }

    suspend fun queryPraises(page: PageBean) =
        safeGetData { MainRetrofitManager.apiService.queryPraises(page.pageNo, page.pageSize) }

    suspend fun queryMyAddedBlogs(page: PageBean) =
        safeGetData { MainRetrofitManager.apiService.queryMyAddedBlogs(page.pageNo, page.pageSize) }

    suspend fun register(phone: String, password: String) =
        safeGetData { MainRetrofitManager.apiService.register(phone, password) }

    suspend fun add(request: AddBlogRequest) =
        safeGetData { MainRetrofitManager.apiService.addBlog(request) }

    suspend fun addComment(request:AddCommentRequest) =
        safeGetData { MainRetrofitManager.apiService.addComment(request) }

    suspend fun addCommentReply(request:AddCommentReplyRequest) =
        safeGetData { MainRetrofitManager.apiService.addCommentReply(request) }

    suspend fun updateBlog(request: EditBlogRequest) =
        safeGetData { MainRetrofitManager.apiService.updateBlog(request) }

    suspend fun deleteBlog(blogId: Int) =
        safeGetData { MainRetrofitManager.apiService.deleteBlog(blogId) }

    suspend fun getById(blogId: Int) =
        safeGetData { MainRetrofitManager.apiService.getBlogById(blogId) }

    suspend fun collect(blogId: Int, type: Int) =
        safeGetData { MainRetrofitManager.apiService.collect(blogId, type) }

    suspend fun praise(blogId: Int, type: Int) =
        safeGetData { MainRetrofitManager.apiService.praise(blogId, type) }

    //获取贴子的评论
    suspend fun getCommentList(blogId:Int) =
        safeGetData { MainRetrofitManager.apiService.getCommentList(blogId) }

    //获取排行榜
    suspend fun getRankingList(page: PageBean) =
        safeGetData { MainRetrofitManager.apiService.getRankingList(page.pageNo, page.pageSize) }

    suspend fun getTest(blogId: Int) =
        safeGetData { MainRetrofitManager.apiService.getBlogById(blogId) }

}