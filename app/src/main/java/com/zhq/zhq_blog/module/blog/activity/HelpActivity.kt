package com.zhq.zhq_blog.module.blog.activity

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import com.zhq.zhq_blog.R
import com.zhq.zhq_blog.base.BaseActivity
import com.zhq.zhq_blog.databinding.ActivityHelpBinding

class HelpActivity : BaseActivity() {
    val mViewBinding by lazy { ActivityHelpBinding.inflate(layoutInflater) }

    private fun initView(){
        mViewBinding.toolbar.title = "帮助"
        setSupportActionBar(mViewBinding.toolbar)
        setWebView()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
    }

    override fun setContentView() {
        setContentView(mViewBinding.root)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.exit_fragment_menu,menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.edit -> finish()
        }
        return true
    }

    private fun setWebView(){
        val webSettings: WebSettings = mViewBinding.docsWebview.settings
        webSettings.javaScriptEnabled = true
        webSettings.domStorageEnabled = true
        webSettings.defaultFontSize = 16

        mViewBinding.docsWebview.isVerticalScrollBarEnabled = false
        mViewBinding.docsWebview.isHorizontalScrollBarEnabled = false
        mViewBinding.docsWebview.webChromeClient = WebChromeClient()
        mViewBinding.docsWebview.loadUrl(
            "file:///android_asset/markdown-cheatsheet-zh.html"
        )
    }
}