package com.zhq.zhq_blog.module.blog.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.jeremyliao.liveeventbus.LiveEventBus
import com.kingja.loadsir.core.LoadService
import com.kingja.loadsir.core.LoadSir
import com.scwang.smartrefresh.layout.api.RefreshLayout
import com.scwang.smartrefresh.layout.listener.OnRefreshLoadMoreListener
import com.zhq.zhq_blog.R
import com.zhq.zhq_blog.base.BaseVMFragment
import com.zhq.zhq_blog.callback.LoadingCallback
import com.zhq.zhq_blog.databinding.FragmentRankingListBinding
import com.zhq.zhq_blog.module.blog.activity.BlogDetailActivity
import com.zhq.zhq_blog.module.blog.adapter.RankingListAdapter
import com.zhq.zhq_blog.module.blog.viewmodel.RankingListFragmentViewModel
import com.zhq.zhq_blog.module.bus.UpdateBlogEvent
import com.zhq.zhq_blog.module.bus.UpdateUserInfoEvent
import com.zhq.zhq_blog.util.CommonLinearItemDecoration
import com.zhq.zhq_blog.util.PageUtil
import com.zhq.zhq_blog.util.RefreshUtil
import com.zhq.zhq_blog.util.zhq.ZHQUtil
import com.zhq.zhq_blog.util.zhq.toast

class RankingListFragment : BaseVMFragment<RankingListFragmentViewModel>() {
    lateinit var mViewBinding: FragmentRankingListBinding
    val adapter:RankingListAdapter by lazy { RankingListAdapter(null) }
    private lateinit var loadService: LoadService<Any>

    override fun createView(inflater: LayoutInflater?, container: ViewGroup?): View {
        mViewBinding = FragmentRankingListBinding.inflate(inflater!!,container,false)
        return mViewBinding.root
    }

    override fun onLazyLoad() {
        getPageList(true)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadService = LoadSir.getDefault().register(
            mViewBinding.smartRefreshLayout
        ){
            loadService.showCallback(LoadingCallback::class.java)
            getPageList(true)
        }
        loadService.showCallback(LoadingCallback::class.java)
        mViewBinding.recyclerView.layoutManager = LinearLayoutManager(requireActivity())
        mViewBinding.recyclerView.addItemDecoration(
            CommonLinearItemDecoration(
                dividerColor = requireContext().getColor(R.color.common_theme),
                dividerHeight = ZHQUtil.dp2px(0.5f)
            )
        )
        mViewBinding.recyclerView.adapter = adapter
        adapter.setOnItemClickListener { _, _, position ->
            BlogDetailActivity.startActivity(
                requireContext(),
                intentBean = BlogDetailActivity.Companion.IntentBean(this.adapter.getItem(position).id)
            )
        }
        mViewBinding.smartRefreshLayout.setEnableLoadMore(true)
        mViewBinding.smartRefreshLayout.setOnRefreshLoadMoreListener(object :
            OnRefreshLoadMoreListener {
            override fun onRefresh(refreshLayout: RefreshLayout) {
                getPageList(true)
            }

            override fun onLoadMore(refreshLayout: RefreshLayout) {
                getPageList(false)
            }

        })
        initBus()


    }

    private fun initBus() {
        //监听事件总线的消息
        LiveEventBus
            .get(UpdateBlogEvent::class.java)
            .observe(this) {
                getPageList(true)
            }
        LiveEventBus
            .get(UpdateUserInfoEvent::class.java)
            .observe(this) {
                getPageList(true)
            }
    }

    fun getPageList(isRefresh: Boolean) {
        mViewModel.getRankingList(PageUtil.getNextServeRankingPageBean(isRefresh, adapter.data.size))
            .observe(viewLifecycleOwner) {
                RefreshUtil.changeRefreshViewStatus(
                    mViewBinding.smartRefreshLayout,
                    it?.data?.dataList?.size ?: 0,
                )
                if (it.isServerResultOK()) {
                    if (isRefresh) {
                        adapter.setI(1)
                        adapter.setNewInstance(it.data?.dataList?.toMutableList())
                    } else {
                        adapter.setI(adapter.data.size+1)
                        adapter.addData(it.data?.dataList!!.toMutableList())
                    }
                } else {
                    toast(it.msg)
                }

                RefreshUtil.changeLoadServiceStatus(it.code, loadService, adapter.data.size)
            }
        mViewModel.livedataComplete.observe(viewLifecycleOwner) {
            mViewBinding.smartRefreshLayout.finishRefresh()
            mViewBinding.smartRefreshLayout.finishLoadMore()
        }
    }
}