package com.zhq.zhq_blog.callback;


import com.kingja.loadsir.callback.Callback;
import com.zhq.zhq_blog.R;


public class ErrorCallback extends Callback {
    @Override
    protected int onCreateView() {
        return R.layout.layout_error;
    }
}
