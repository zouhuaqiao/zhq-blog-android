package com.zhq.zhq_blog.base

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.zhq.zhq_blog.net.common.CommonResultBean
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

/**
 *
 */
open class BaseViewModel : ViewModel() {
    //所有通用的事件
    val livedataEvent = MutableLiveData<BaseVMEvent>()

    /**
     * 创建并执行协程
     * @param workBlock [@kotlin.ExtensionFunctionType] SuspendFunction1<CoroutineScope, Unit> 获取数据，拿到结果并处理
     * @param handleLoading Boolean 是否处理loading框 默认处理
     * @param loadingMsg String?  loading框的显示文字 不传也没事，app有默认文字
     * @return Job
     */
    fun launch(
        workBlock: suspend CoroutineScope.() -> Unit,
        handleLoading: Boolean = true,
        loadingMsg: String? = null
    ): Job {
        if (handleLoading) {
            livedataEvent.value = BaseVMEvent(EventType.SHOW_LOADING, loadingMsg)
        }
        var job = viewModelScope.launch {
            workBlock()
            if (handleLoading) {
                livedataEvent.value = BaseVMEvent(EventType.HIDE_LOADING)
            }
        }
        return job
    }

    fun handleResult(
        response: CommonResultBean<*>,
        successBlock: () -> Unit,
        failBlock: () -> Unit
    ) {
        if (response.isServerResultOK()) {
            successBlock()
        } else {
            failBlock()
        }
    }
}