package com.zhq.zhq_blog.base

import android.app.Activity
import android.content.Context
import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gyf.immersionbar.ImmersionBar
import com.zhq.zhq_blog.R
import com.zhq.zhq_blog.util.ActivityUtils
import com.zhq.zhq_blog.util.dialog.DialogUtil
import com.zhq.zhq_blog.util.log.LogUtil


/**
 *
 */
abstract class BaseActivity : AppCompatActivity(), ILoading {
    lateinit var context: Context
    lateinit var activity: Activity
    private lateinit var loadingDialog: DialogUtil
    //子类想做事情就复写此方法
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ActivityUtils.screenManager.pushActivity(this)
        context = this
        activity = this
        initStatusColor()
        loadingDialog = DialogUtil(activity)
        //因为大部分界面在没有特意适配横屏时，自动适配效果根本不理想，所以直接强制竖屏，有特殊需要自己再设置
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
        setContentView()
    }

    /**
     * 因为系统自带的设置view方法有很多种,id,view都可以.所以这里不像
     * 大家都用id的封装方式,那是限制了自己
     * 建议用viewBinding的方式。个人不喜欢dataBinding
     */
    protected abstract fun setContentView()

    /**
     * 解析传递的固定格式的bean。这样不用一个一个参数的传。而且需要修改参数时方便提示
     * @return T?
     */
    fun <T : BaseIntentBean> parseIntent(): T? {
        //获取参数
        val bean: T? = intent.getSerializableExtra(Constant.IntentKey.IntentBean) as T?
        return if (bean != null) {
            LogUtil.i("传递过来的参数:$bean")
            bean
        } else {
            LogUtil.i("没传过来参数")
            null
        }
    }
    /**
     * 默认设置一下状态栏颜色,如果不满意可以自己再设置一遍
     */
    private fun initStatusColor() {
        ImmersionBar.with(this) //状态栏颜色
            .statusBarColor(R.color.common_bg_white) //状态栏文字颜色
            .statusBarDarkFont(true)
            .fitsSystemWindows(true) //使用该属性必须指定状态栏的颜色，不然状态栏透明，很难看. false表示布局嵌入状态栏。true表示布局避开状态栏
            .init()
    }
    override fun showLoading(msg: String) {
        loadingDialog.showLoading(msg)
    }

    override fun hideLoading() {
        loadingDialog.dismissLoading()
    }

    override fun onDestroy() {
        super.onDestroy()
        hideLoading()
        ActivityUtils.screenManager.popActivity(this)
    }

}