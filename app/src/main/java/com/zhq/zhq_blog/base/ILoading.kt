package com.zhq.zhq_blog.base


/**
 * 作用:加载对话框的方法定义
 */
interface ILoading {
    fun showLoading(msg: String = "")

    fun hideLoading()
}