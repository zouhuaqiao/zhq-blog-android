package com.zhq.zhq_blog.base

import java.io.Serializable

/**
 * 默认的activity传递bean
 */
open class BaseIntentBean() : Serializable {}