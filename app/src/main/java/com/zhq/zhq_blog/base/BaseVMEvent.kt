package com.zhq.zhq_blog.base

/**
 *
 */
enum class EventType {
    SHOW_LOADING,
    HIDE_LOADING,
}

data class BaseVMEvent(var eventType: EventType, var data: Any? = null)