package com.zhq.zhq_blog.base

/**
 * 作用:所有的常量类。这里每个常量不用大写表示，因为个人不喜欢，看大写字母一眼看不明白
 * 注意事项:采用kotlin object方式是因为这样的话既能引用,也能写逻辑代码.
 */
object Constant {
    object Url {
        /**
         * 这个url是gradle中配置的。可以通过指定打包方式选择不同的值
         * 网络请求的baseUrl
         */
        var request_base_url: String = "http://121.41.169.101:10001" //阿里云服务器
//        var request_base_url: String = "http://10.0.2.2:10001" //模拟器服务器
//        var request_base_url: String = "http://172.21.45.223:10001" //本地服务器1
//        var request_base_url: String = "http://172.24.81.79:10001" //本地服务器2
    }
    /**
     * intent传递字段
     */
    object IntentKey {
        /**
         * 默认activity传递对象时的key
         */
        const val IntentBean = "IntentBean"

        /**
         * 默认activity返回对象时的key
         */
        const val response_IntentBean = "response_IntentBean"

        /**
         * 启动博客详情activity时是否点击comment按钮
         */
        const val isClickComment = "isClickComment"
    }
    /**
     * Tag
     */
    object Tag {
        /**
         * 打印log的tag
         */
        const val tag = "邹博客"
    }
    /**
     * sp相关的常量
     */
    object SP {
        object UserInfo{
            /**
             * sp名字
             * 用户信息相关
             */
            const val spName = "userInfo"
            object Key{
                /**
                 * 数据的key
                 */
                const val data = "data"
            }
        }
        object AppStatus{
            /**
             * sp名字
             * 用户信息相关
             */
            const val spName = "AppStatus"
            object Key{
                /**
                 * 数据的key
                 */
                const val splash = "splash"
            }
        }

    }

    const val MY_EMAIL = "zouhuaqiao20@outlook.com"
    const val MY_GITHUB = "https://gitee.com/zouhuaqiao"
    const val MY_ZHIHU = "https://www.zhihu.com/people/zouhuaqiao"
    const val PROJECT_PAGE_URL = "https://gitee.com/zouhuaqiao/zhq-blog-android"

}