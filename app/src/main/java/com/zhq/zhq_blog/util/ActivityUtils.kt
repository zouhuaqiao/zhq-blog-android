package com.zhq.zhq_blog.util

import android.app.Activity
import java.util.*

/**
 * 活动栈，管理活动
 */
class ActivityUtils private constructor() {
    // 弹出当前activity并销毁
    fun popActivity(activity: Activity?) {
        if (activity != null) {
            activity.finish()
            mActivityStack.remove(activity)
        }
    }

    // 将当前Activity推入栈中
    fun pushActivity(activity: Activity) {
        mActivityStack.add(activity)
    }

    // 退出栈中所有Activity
    fun clearAllActivity() {
        while (!mActivityStack.isEmpty()) {
            val activity = mActivityStack.pop()
            activity?.finish()
        }
    }

    companion object {
        private val mActivityStack = Stack<Activity>()
        val screenManager = ActivityUtils()
    }
}