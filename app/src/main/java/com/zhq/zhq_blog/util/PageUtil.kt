package com.zhq.zhq_blog.util

/**
 *
 */

const val SERVER_FIRST_PAGE = 1
const val COMMON_PAGE_SIZE = 10
const val RANKING_PAGE_SIZE = 20

object PageUtil {
    //获取下一页数据
    private fun getCurrentPage(totalListCount: Int, pageSize: Int = COMMON_PAGE_SIZE): Int {
        val currentPage: Int = if (totalListCount % pageSize == 0) {
            totalListCount / pageSize
        } else {
            (totalListCount / pageSize) + 1
        }
        return currentPage
    }

    fun getNextServerPage(isRefresh: Boolean, totalListCount: Int, pageSize: Int = 20): Int {
        return if (isRefresh) {
            SERVER_FIRST_PAGE
        } else {
            val currentPage = getCurrentPage(totalListCount, pageSize)
            currentPage + 1
        }
    }

    fun isLocalDefaultPage(totalListCount: Int): Boolean {
        return totalListCount == 0
    }

    //获取博客pageBean
    fun getNextServerPageBean(isRefresh: Boolean, totalListCount: Int, pageSize: Int = COMMON_PAGE_SIZE): PageBean {
        val pageBean = PageBean()
        if (isRefresh) {
            pageBean.pageNo = SERVER_FIRST_PAGE
            pageBean.pageSize = pageSize
        } else {
            val currentPage = getCurrentPage(totalListCount, pageSize)
            pageBean.pageNo = currentPage + 1
            pageBean.pageSize = pageSize
        }
        return pageBean
    }

    //获取排行榜pageBean
    fun getNextServeRankingPageBean(isRefresh: Boolean, totalListCount: Int, pageSize: Int = RANKING_PAGE_SIZE): PageBean {
        val pageBean = PageBean()
        if (isRefresh) {
            pageBean.pageNo = SERVER_FIRST_PAGE
            pageBean.pageSize = pageSize
        } else {
            val currentPage = getCurrentPage(totalListCount, pageSize)
            pageBean.pageNo = currentPage + 1
            pageBean.pageSize = pageSize
        }
        return pageBean
    }
}

class PageBean {
    var pageNo: Int = SERVER_FIRST_PAGE
    var pageSize: Int = COMMON_PAGE_SIZE
}