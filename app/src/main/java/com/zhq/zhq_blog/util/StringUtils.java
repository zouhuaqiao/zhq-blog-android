package com.zhq.zhq_blog.util;

import android.text.TextUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 */
public class StringUtils {

    public static boolean isEmail(String string) {
        if (string == null)
            return false;
        String regEx1 = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
        Pattern p;
        Matcher m;
        p = Pattern.compile(regEx1);
        m = p.matcher(string);
        return m.matches();
    }

    //座机
    public static boolean isFixedPhone(String string) {
        if (string == null)
            return false;
        //将点号全部替换为横线
        String targetStr = string.replaceAll("\\-", "");
        String regEx1 = "^(010|02\\d|0[3-9]\\d{2})?\\d{6,8}$";
        Pattern p;
        Matcher m;
        p = Pattern.compile(regEx1);
        m = p.matcher(targetStr);
        return m.matches();
    }

    public static boolean isMobilePhone(String string) {
        if (string == null)
            return false;
        String regEx1 = "^1[34578][0-9]{9}$";
        Pattern p;
        Matcher m;
        p = Pattern.compile(regEx1);
        m = p.matcher(string);
        return m.matches();
    }

    /**
     * 格式化为两位小数字符串
     *
     */
    public static String format(Double d) {
        return String.format("%.2f", d);
    }

    public static String formatBankAccount(String account) {
        return account.replaceAll("\\d{4}(?!$)", "$0  ");
    }

    /**
     * 将手机号码18033339999 转换为 180****9999
     *
     */
    public static String changPhoneNumber(String phoneNumber) {
        if (TextUtils.isEmpty(phoneNumber)) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        if (phoneNumber.length() > 10) {
            String frontThreeString = phoneNumber.substring(0, 3);
            sb.append(frontThreeString);
            String substring = phoneNumber.substring(3, 7);
            String replace = substring.replace(substring, "****");
            sb.append(replace);
            String lastFourString = phoneNumber.substring(7, 11);
            sb.append(lastFourString);
            return sb.toString();
        } else {
            return "";
        }

    }

    /**
     * 分隔11位的手机号 185 0123 1486
     *
     */
    public static String splitPhoneNumber(String phoneNumber) {
        if (TextUtils.isEmpty(phoneNumber)) {
            return "";
        }
        if (phoneNumber.length() != 11) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        phoneNumber.length();
        String frontThreeString = phoneNumber.substring(0, 3);
        sb.append(frontThreeString);
        sb.append(" ");
        String substring = phoneNumber.substring(3, 7);
        sb.append(substring);
        sb.append(" ");
        String lastFourString = phoneNumber.substring(7, 11);
        sb.append(lastFourString);
        return sb.toString();

    }

    /**
     * 将身份证号130922199104206011 转换为 1***************1
     * 微信就是这样显示的
     *
     * @param idCardNumber
     * @return
     */
    public static String changIdCardNumber(String idCardNumber) {
        if (TextUtils.isEmpty(idCardNumber)) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        if (idCardNumber.length() > 2) {
            String firstStr = idCardNumber.substring(0, 1);
            sb.append(firstStr);
            String substring = idCardNumber.substring(1, idCardNumber.length() - 1);
            StringBuilder sbXing = new StringBuilder();
            for (char ignored : substring.toCharArray()) {
                sbXing.append("*");
            }
            String replace = substring.replace(substring, sbXing);
            sb.append(replace);
            String lastStr = idCardNumber.substring(idCardNumber.length() - 1);
            sb.append(lastStr);
            return sb.toString();
        } else {
            return idCardNumber;
        }

    }

    public static int containCount(String all, String s) {
        int count = 0;
        while (all.contains(s)) {
            all = all.substring(all.indexOf(s) + 1);
            ++count;

        }
        return count;
    }
}
