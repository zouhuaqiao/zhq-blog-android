package com.zhq.zhq_blog.util.dialog;

import android.app.Activity;

/**
 *
 */
public class DialogUtil {
    private final CustomDialog progressDialog;

    public DialogUtil(Activity activity) {
        progressDialog = new CustomDialog(activity);
    }

    public  void showLoading(String msg) {
        progressDialog.show();
        progressDialog.setMessage(msg);
        progressDialog.startAnimation();
    }

    public  void dismissLoading() {
        if (progressDialog.isShowing()) {
            progressDialog.stopAnimation();
            progressDialog.dismiss();
        }

    }
}
