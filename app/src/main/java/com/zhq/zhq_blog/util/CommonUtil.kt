package com.zhq.zhq_blog.util

import android.app.Activity
import android.view.WindowManager
import android.widget.EditText

/**
 * EditText获取焦点并显示软键盘
 */
fun showKeyboard(activity: Activity, editText: EditText) {
    editText.isFocusable = true
    editText.isFocusableInTouchMode = true
    editText.requestFocus()
    activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
}