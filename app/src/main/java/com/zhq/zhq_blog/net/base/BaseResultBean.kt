package com.zhq.zhq_blog.net.base

/**
 * 作用:这里只定义需要的必要信息.其他自定义json都由app自己实现
 */
interface BaseResultBean {
    fun getAppCode(): Int
    fun getAppMsg(): String

    /**
     * 判断结果是否正确
     * 因为后台人员有各种告知正确的方法,不一定是通过appcode
     */
    fun isServerResultOK(): Boolean
}