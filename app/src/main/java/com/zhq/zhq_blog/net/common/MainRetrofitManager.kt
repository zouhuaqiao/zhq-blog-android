package com.zhq.zhq_blog.net.common

import com.zhq.zhq_blog.base.Constant
import com.zhq.zhq_blog.net.base.RetrofitManager
import okhttp3.Interceptor

/**
 * 作用:用户网络类
 * 如果有其他host,可以新建一个同样的类管理
 * 注意事项:
 */
object MainRetrofitManager : RetrofitManager<MainApi>() {
    override fun getAppInterceptors(): List<Interceptor> {
        return listOf<Interceptor>(AppInterceptor())
    }

    override fun getBaseUrl(): String {
        return Constant.Url.request_base_url
    }
}