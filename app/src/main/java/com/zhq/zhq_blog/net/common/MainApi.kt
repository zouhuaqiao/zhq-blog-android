package com.zhq.zhq_blog.net.common

import com.zhq.zhq_blog.module.blog.bean.*
import com.zhq.zhq_blog.module.user.bean.LoginResult
import retrofit2.http.*

interface MainApi {

    @POST("/user/login")
    suspend fun login(
        @Query("phone") phone: String,
        @Query("password") password: String
    ): CommonResultBean<LoginResult>

    @GET("/user/user_info")
    suspend fun userInfo(): CommonResultBean<LoginResult>

    @POST("/user/register")
    suspend fun register(
        @Query("phone") phone: String,
        @Query("password") password: String
    ): CommonResultBean<*>

    @POST("/user/icon_url")
    suspend fun iconUrl(@Query("iconUrl") iconUrl: String): CommonResultBean<*>

    @POST("/user/change_username")
    suspend fun changeUsername(@Query("username") username: String): CommonResultBean<*>

    @POST("/user/change_password")
    suspend fun changePassword(@Query("password") password: String): CommonResultBean<*>

    @POST("/blog/query_page")
    suspend fun blogPageList(
        @Query("pageNo") pageNo: Int,
        @Query("pageSize") pageSize: Int
    ): CommonResultBean<BlogPageListResult>

    @POST("/blog/query_title")
    suspend fun searchBlog(
        @Query("pageNo") pageNo: Int,
        @Query("pageSize") pageSize: Int,
        @Query("keywords") keywords: String
    ): CommonResultBean<BlogPageListResult>

    @POST("/blog/query_collects")
    suspend fun queryCollects(
        @Query("pageNo") pageNo: Int,
        @Query("pageSize") pageSize: Int,
    ): CommonResultBean<BlogPageListResult>

    @POST("/blog/query_praises")
    suspend fun queryPraises(
        @Query("pageNo") pageNo: Int,
        @Query("pageSize") pageSize: Int,
    ): CommonResultBean<BlogPageListResult>

    @POST("/blog/query_my_added")
    suspend fun queryMyAddedBlogs(
        @Query("pageNo") pageNo: Int,
        @Query("pageSize") pageSize: Int,
    ): CommonResultBean<BlogPageListResult>

    @POST("/blog/add")
    suspend fun addBlog(@Body request: AddBlogRequest): CommonResultBean<Int>

    @POST("blog/comment/add_comment")
    suspend fun addComment(@Body request: AddCommentRequest): CommonResultBean<Int>

    @POST("blog/comment/add_comment_reply")
    suspend fun addCommentReply(@Body request: AddCommentReplyRequest): CommonResultBean<Int>

    @POST("/blog/update")
    suspend fun updateBlog(@Body request: EditBlogRequest): CommonResultBean<*>

    @POST("/blog/delete/{blogId}")
    suspend fun deleteBlog(@Path("blogId") blogId: Int): CommonResultBean<*>

    @GET("/blog/queryById/{blogId}")
    suspend fun getBlogById(@Path("blogId") blogId: Int): CommonResultBean<BlogPageListResult.Data>

    @POST("/blog/collect")
    suspend fun collect(@Query("blogId") blogId: Int, @Query("type") type: Int): CommonResultBean<*>

    @POST("/blog/praise")
    suspend fun praise(@Query("blogId") blogId: Int, @Query("type") type: Int): CommonResultBean<*>

    @GET("/blog/comment/queryCommentList/{blogId}")
    suspend fun getCommentList(@Path("blogId") blogId: Int): CommonResultBean<CommentBeanResult.Data>

    @POST("/blog/rankingList")
    suspend fun getRankingList(
        @Query("pageNo") pageNo: Int,
        @Query("pageSize") pageSize: Int
    ): CommonResultBean<BlogPageListResult>

}