
# 预览

<img src="https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/d3c892e3db284679abf360632d014e0b~tplv-k3u1fbpfcp-watermark.image?" width = "30%" />
<img src="https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/536ebd36d9074fbc9a02b1923d7fd542~tplv-k3u1fbpfcp-watermark.image?"  width = "30%"/>
<img src="https://gitee.com/zouhuaqiao/warehouse/raw/master/image/1650596612911.jpg"  width = "30%" />
<img src="https://gitee.com/zouhuaqiao/warehouse/raw/master/image/1650596612917.jpg" width = "30%"  />
<img src="https://gitee.com/zouhuaqiao/warehouse/raw/master/image/1650596612921.jpg" width = "30%"  />
<img src="https://gitee.com/zouhuaqiao/warehouse/raw/master/image/1650596612928.jpg" width = "30%"  />
<img src="https://gitee.com/zouhuaqiao/warehouse/raw/master/image/1650596612932.jpg" width = "30%"  />
<img src="https://gitee.com/zouhuaqiao/warehouse/raw/master/image/1650596612936.jpg" width = "30%"  />

# 介绍
本项目是做毕设时开发的Android+springboot的博客系统，主要就是发博客，集成了markdown文档的显示与编辑。
本项目是二次开发，鸣谢原作者刘传政，原项目地址：https://gitee.com/liuchuanzheng/LCZ_Blog_Android
# 代码技术
## Android
- kotlin
- MVVM
- livedata
- [SmartRefreshLayout](https://link.juejin.cn/?target=https%3A%2F%2Fhub.fastgit.org%2Fscwang90%2FSmartRefreshLayout "https://hub.fastgit.org/scwang90/SmartRefreshLayout")
- [logger](https://link.juejin.cn/?target=https%3A%2F%2Fhub.fastgit.org%2Forhanobut%2Flogger "https://hub.fastgit.org/orhanobut/logger")
- [AndroidUtilCode](https://link.juejin.cn/?target=https%3A%2F%2Fhub.fastgit.org%2FBlankj%2FAndroidUtilCode%2Fblob%2Fmaster%2FREADME-CN.md "https://hub.fastgit.org/Blankj/AndroidUtilCode/blob/master/README-CN.md")
- [ImmersionBar](https://link.juejin.cn/?target=https%3A%2F%2Fhub.fastgit.org%2Fgyf-dev%2FImmersionBar "https://hub.fastgit.org/gyf-dev/ImmersionBar")
- [LoadSir](https://link.juejin.cn/?target=https%3A%2F%2Fgithub.com%2FKingJA%2FLoadSir "https://github.com/KingJA/LoadSir")
- [BaseRecyclerViewAdapterHelper](https://link.juejin.cn/?target=https%3A%2F%2Fgithub.com%2FCymChad%2FBaseRecyclerViewAdapterHelper "https://github.com/CymChad/BaseRecyclerViewAdapterHelper")
- [lottie](https://link.juejin.cn/?target=https%3A%2F%2Fgithub.com%2Fairbnb%2Flottie-android "https://github.com/airbnb/lottie-android")
- [LiveEventBus](https://link.juejin.cn/?target=https%3A%2F%2Fgithub.com%2FJeremyLiao%2FLiveEventBus "https://github.com/JeremyLiao/LiveEventBus")
- CircleImageView
- glide
- retrofit+okhttp+协程
## java后台
- springboot2.6.2
- mysql
- druid
- swagger
- lombok
- mybatis-plus
- mybatis-plus 代码自动生成器
- jwt
- 统一接口返回对象
- token拦截器
- 异常拦截器
- jackson
- 阿里云单体服务器部署
# 功能
- 登录、注册、退出
- 修改用户名、头像
- 发布、删除、修改、关注、查看、搜索博客
涉及到了sql的一对一，一对多，多对多，分页等全部情况
# 代码
## Android
https://gitee.com/zouhuaqiao/zhq-blog-android
## java后台
代码中包含只表结构的sql文件和带一点数据的sql文件  
https://gitee.com/zouhuaqiao/zhq-blog-java
# 接口文档
http://zouhuaqiao.xyz:10001/swagger-ui.html#
# 安装包
https://gitee.com/zouhuaqiao/zhq-blog-android/tree/master/apk